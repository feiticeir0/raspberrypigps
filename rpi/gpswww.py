# Feiticeir0 - Bruno Santos (feiticeir0@whatgeek.com.pt)
# date: 10.12.2014
# 
# Python script para guardar as coordenadas
# correntes num ficheiro para ser lido via script em PHP
# de um computador remoto

from sys import argv
import gps
import gpxpy
import gpxpy.gpx


#Listen on port 2947 of gpsd
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

while True:
	try:
		report = session.next()
			# wait for a 'TPV' report and display the current time
			# to see all report data, uncomment the line below
			#print report
		if report['class'] == 'TPV':
			# Abrir ficheiro para guardar as coordenadas
			coords = open ("www/coords.txt","w+")
			coords.write(str(report.lat))
			coords.write(",");
			coords.write(str(report.lon))
			coords.close()
	except KeyError:
		pass
	except KeyboardInterrupt:
		quit()
	except StopIteration:
		session = None
		print "GPSD has terminated"

