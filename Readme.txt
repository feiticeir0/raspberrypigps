Conjunto de scripts para reportar as coordenadas correntes de um PI para ser
lido por outro script e mostrar num mapa

Os ficheiros são os seguintes:
	- RPITracking.html
		Este é o ficheiro que comanda todos os restantes. Além de
		verificar a conetividade, também vai ler remotamente as
		coordenadas.
		- Adicionado campo para colocar endereço IP do RPI
	- getcoords.php
		Ficheiro que vai ler remotamente - via curl - as coordendas ao
		Raspberry PI
	- pingPHP.php
		- Ficheiro que verificar a conetividade com o Raspberry PI

Estes ficheiros tem que ser executados num computador com servidor web -
apache ou nginx com suporte de PHP


Não é necessário alterar mais nada nestes ficheiros.

	Diretoria rpi
		- Nesta diretoria encontra-se o ficheiro em Python para
		  colocar no Raspberry PI
		  - Uma vez no Raspberry PI (numa diretoria na home do
		    utilizador - pode ser /home/pi/gps), criar a diretoria www
		    (nas intruções no blog).

		  - Para executar, executar como:
		  	sudo python gpswww.py


